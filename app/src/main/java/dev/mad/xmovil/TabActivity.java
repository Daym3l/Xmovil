package dev.mad.xmovil;

import android.app.Activity;

import android.content.pm.ActivityInfo;
import android.graphics.Color;

import android.net.Uri;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import java.io.File;
import java.util.ArrayList;
import java.util.StringTokenizer;

import dev.mad.xmovil.db.MovilDbHelper;
import dev.mad.xmovil.domains.Moviles;

public class TabActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    protected SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    protected ViewPager mViewPager;
    private LinearLayout llsliderDotsPnael;
    private int dotsCouts;
    protected String model1;
    protected String model2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        model1 = getIntent().getStringExtra("model1");
        model2 = getIntent().getStringExtra("model2");

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        llsliderDotsPnael = (LinearLayout) findViewById(R.id.SliderDost);

        mViewPager.setAdapter(mSectionsPagerAdapter);
        dotsCouts = mSectionsPagerAdapter.getCount();
        final ImageView[] dots = new ImageView[dotsCouts];

        for (int i = 0; i < dotsCouts; i++) {
            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.non_active));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(8, 0, 8, 0);
            llsliderDotsPnael.addView(dots[i], params);

        }
        mViewPager.setCurrentItem(1);
        dots[mViewPager.getCurrentItem()].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active));

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                for (int i = 0; i < dotsCouts; i++) {
                    dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.non_active));
                }
                dots[position].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }


    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        protected int rec1;
        protected int rec2;
        protected String model1;
        protected String model2;
        protected String modelo1;
        protected String modelo2;
        protected String name1;
        protected String name2;

        static MovilDbHelper miBBDDHelper;
        public java.util.List<Moviles> ITEMS = new ArrayList<Moviles>();
        public java.util.List<Moviles> ITEMS2 = new ArrayList<Moviles>();
        private static final String ARG_SECTION_NUMBER = "section_number";
        TextView model_device1;
        TextView name_device1;
        TextView year_device1;
        TextView model_device2;
        TextView name_device2;
        TextView year_device2;
        TextView dimensiones1;
        TextView dimensiones2;
        TextView grosor1;
        TextView grosor2;
        TextView peso1;
        TextView peso2;
        TextView dual1;
        TextView dual2;
        TextView pantalla1;
        TextView pantalla2;
        TextView tpantalla1;
        TextView tpantalla2;
        TextView dpi1;
        TextView dpi2;
        TextView procesador1;
        TextView procesador2;
        TextView ram1;
        TextView ram2;
        TextView interna1;
        TextView interna2;
        TextView sd1;
        TextView sd2;
        TextView bateria1;
        TextView bateria2;
        TextView camaraf1;
        TextView camaraf2;
        TextView camaraT1;
        TextView camaraT2;
        TextView sistema1;
        TextView sistema2;
        TextView version1;
        TextView version2;
        TextView precio1;
        TextView precio2;
        TextView result1;
        TextView result2;
        ImageView image1;
        ImageView image2;


        public PlaceholderFragment() {

        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber, String m1, String m2) {

            PlaceholderFragment fragment = new PlaceholderFragment();
            fragment.model1 = m1;
            fragment.model2 = m2;
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = null;
            if (crearBBDD(this.getActivity())) {
                miBBDDHelper.abrirBaseDatos();
                ITEMS = miBBDDHelper.searchMovil(model1, model2);
                miBBDDHelper.close();

            } else {
                Toast toast = Toast.makeText(getContext(), "Error al verificar la base de datos.", Toast.LENGTH_SHORT);
                toast.show();
            }
            getModels();
            if (rutarExist()) {
                rootView = getView(inflater, container, rootView);
            } else {
                rootView = getViewalternativo(inflater, container, rootView);
            }

            return rootView;
        }

        private View getView(LayoutInflater inflater, ViewGroup container, View rootView) {
            if (getArguments().getInt(ARG_SECTION_NUMBER) == 1) {
                rootView = inflater.inflate(R.layout.fragment_left__image, container, false);
                image1 = (ImageView) rootView.findViewById(R.id.imageDevice1);
                TextView cabecera = (TextView) rootView.findViewById(R.id.tv_modeloleft);
                cabecera.setText(name1);
                getImageForView(modelo1, image1);

            }
            if (getArguments().getInt(ARG_SECTION_NUMBER) == 3) {
                rootView = inflater.inflate(R.layout.fragment_right__image, container, false);
                image2 = (ImageView) rootView.findViewById(R.id.imageDevice2);
                TextView cabecera2 = (TextView) rootView.findViewById(R.id.tv_modeloRight);
                cabecera2.setText(name2);
                getImageForView(modelo2, image2);

            }
            if (getArguments().getInt(ARG_SECTION_NUMBER) == 2) {
                rootView = inflater.inflate(R.layout.fragment_tab, container, false);
                model_device1 = (TextView) rootView.findViewById(R.id.tv_model_dispositivo1);
                name_device1 = (TextView) rootView.findViewById(R.id.tv_name_device1);
                year_device1 = (TextView) rootView.findViewById(R.id.tv_anno_dispostivo1);
                model_device2 = (TextView) rootView.findViewById(R.id.tv_model_dispositivo2);
                name_device2 = (TextView) rootView.findViewById(R.id.tv_name_device2);
                year_device2 = (TextView) rootView.findViewById(R.id.tv_anno_dispositivo2);
                dimensiones1 = (TextView) rootView.findViewById(R.id.tv_tamanno1);
                dimensiones2 = (TextView) rootView.findViewById(R.id.tv_tamanno2);
                grosor1 = (TextView) rootView.findViewById(R.id.tv_grosor1);
                grosor2 = (TextView) rootView.findViewById(R.id.tv_grosor2);
                peso1 = (TextView) rootView.findViewById(R.id.tv_pseo1);
                peso2 = (TextView) rootView.findViewById(R.id.tv_pseo2);
                dual1 = (TextView) rootView.findViewById(R.id.tv_dual1);
                dual2 = (TextView) rootView.findViewById(R.id.tv_dual2);
                pantalla1 = (TextView) rootView.findViewById(R.id.tv_pantalla1);
                pantalla2 = (TextView) rootView.findViewById(R.id.tv_pantalla2);
                tpantalla1 = (TextView) rootView.findViewById(R.id.tv_tpanel1);
                tpantalla2 = (TextView) rootView.findViewById(R.id.tv_panel2);
                dpi1 = (TextView) rootView.findViewById(R.id.tv_dpi1);
                dpi2 = (TextView) rootView.findViewById(R.id.tv_dpi2);
                procesador1 = (TextView) rootView.findViewById(R.id.tv_nucleo1);
                procesador2 = (TextView) rootView.findViewById(R.id.tv_nucleo2);
                ram1 = (TextView) rootView.findViewById(R.id.tv_ram1);
                ram2 = (TextView) rootView.findViewById(R.id.tv_ram2);
                interna1 = (TextView) rootView.findViewById(R.id.tv_interna1);
                interna2 = (TextView) rootView.findViewById(R.id.tv_interna2);
                sd1 = (TextView) rootView.findViewById(R.id.tv_micro1);
                sd2 = (TextView) rootView.findViewById(R.id.tv_micro2);
                bateria1 = (TextView) rootView.findViewById(R.id.tv_bateria1);
                bateria2 = (TextView) rootView.findViewById(R.id.tv_bateria2);
                camaraf1 = (TextView) rootView.findViewById(R.id.tv_frontal1);
                camaraf2 = (TextView) rootView.findViewById(R.id.tv_frontal2);
                camaraT1 = (TextView) rootView.findViewById(R.id.tv_trasera1);
                camaraT2 = (TextView) rootView.findViewById(R.id.tv_trasera2);
                sistema1 = (TextView) rootView.findViewById(R.id.tv_sistema1);
                sistema2 = (TextView) rootView.findViewById(R.id.tv_sistema2);
                version1 = (TextView) rootView.findViewById(R.id.tv_vsistema1);
                version2 = (TextView) rootView.findViewById(R.id.tv_vsistema2);
//                precio1 = (TextView) rootView.findViewById(R.id.tv_precio1);
//                precio2 = (TextView) rootView.findViewById(R.id.tv_precio2);
                result1 = (TextView) rootView.findViewById(R.id.tv_result1);
                result2 = (TextView) rootView.findViewById(R.id.tv_result2);
                setValores();

            }
            return rootView;
        }

        private View getViewalternativo(LayoutInflater inflater, ViewGroup container, View rootView) {


            rootView = inflater.inflate(R.layout.fragment_tab, container, false);
            model_device1 = (TextView) rootView.findViewById(R.id.tv_model_dispositivo1);
            name_device1 = (TextView) rootView.findViewById(R.id.tv_name_device1);
            year_device1 = (TextView) rootView.findViewById(R.id.tv_anno_dispostivo1);
            model_device2 = (TextView) rootView.findViewById(R.id.tv_model_dispositivo2);
            name_device2 = (TextView) rootView.findViewById(R.id.tv_name_device2);
            year_device2 = (TextView) rootView.findViewById(R.id.tv_anno_dispositivo2);
            dimensiones1 = (TextView) rootView.findViewById(R.id.tv_tamanno1);
            dimensiones2 = (TextView) rootView.findViewById(R.id.tv_tamanno2);
            grosor1 = (TextView) rootView.findViewById(R.id.tv_grosor1);
            grosor2 = (TextView) rootView.findViewById(R.id.tv_grosor2);
            peso1 = (TextView) rootView.findViewById(R.id.tv_pseo1);
            peso2 = (TextView) rootView.findViewById(R.id.tv_pseo2);
            dual1 = (TextView) rootView.findViewById(R.id.tv_dual1);
            dual2 = (TextView) rootView.findViewById(R.id.tv_dual2);
            pantalla1 = (TextView) rootView.findViewById(R.id.tv_pantalla1);
            pantalla2 = (TextView) rootView.findViewById(R.id.tv_pantalla2);
            tpantalla1 = (TextView) rootView.findViewById(R.id.tv_tpanel1);
            tpantalla2 = (TextView) rootView.findViewById(R.id.tv_panel2);
            dpi1 = (TextView) rootView.findViewById(R.id.tv_dpi1);
            dpi2 = (TextView) rootView.findViewById(R.id.tv_dpi2);
            procesador1 = (TextView) rootView.findViewById(R.id.tv_nucleo1);
            procesador2 = (TextView) rootView.findViewById(R.id.tv_nucleo2);
            ram1 = (TextView) rootView.findViewById(R.id.tv_ram1);
            ram2 = (TextView) rootView.findViewById(R.id.tv_ram2);
            interna1 = (TextView) rootView.findViewById(R.id.tv_interna1);
            interna2 = (TextView) rootView.findViewById(R.id.tv_interna2);
            sd1 = (TextView) rootView.findViewById(R.id.tv_micro1);
            sd2 = (TextView) rootView.findViewById(R.id.tv_micro2);
            bateria1 = (TextView) rootView.findViewById(R.id.tv_bateria1);
            bateria2 = (TextView) rootView.findViewById(R.id.tv_bateria2);
            camaraf1 = (TextView) rootView.findViewById(R.id.tv_frontal1);
            camaraf2 = (TextView) rootView.findViewById(R.id.tv_frontal2);
            camaraT1 = (TextView) rootView.findViewById(R.id.tv_trasera1);
            camaraT2 = (TextView) rootView.findViewById(R.id.tv_trasera2);
            sistema1 = (TextView) rootView.findViewById(R.id.tv_sistema1);
            sistema2 = (TextView) rootView.findViewById(R.id.tv_sistema2);
            version1 = (TextView) rootView.findViewById(R.id.tv_vsistema1);
            version2 = (TextView) rootView.findViewById(R.id.tv_vsistema2);
//                precio1 = (TextView) rootView.findViewById(R.id.tv_precio1);
//                precio2 = (TextView) rootView.findViewById(R.id.tv_precio2);
            result1 = (TextView) rootView.findViewById(R.id.tv_result1);
            result2 = (TextView) rootView.findViewById(R.id.tv_result2);
            setValores();


            return rootView;
        }

        public static boolean crearBBDD(Activity activity) {
            miBBDDHelper = new MovilDbHelper(activity);
            return miBBDDHelper.comprobarBaseDatos();
        }

        public boolean rutarExist() {
            boolean flag = false;
            File ruta_SD = Environment.getExternalStorageDirectory();
            File imgFile = new File(ruta_SD, "/XMOVIL/moviles/");
            if (imgFile.exists()) {
                flag = true;
            }
            return flag;
        }

        public void getImageForView(String model, ImageView vista) {
            File ruta_SD = Environment.getExternalStorageDirectory();
            File imgFile = new File(ruta_SD, "/XMOVIL/moviles/" + model + ".jpg");
            Uri file = Uri.fromFile(imgFile);
            if (rutarExist()) {
                vista.setImageURI(file);
            } else {
                vista.setImageResource(R.drawable.placeholder);
            }


        }

        public void getModels() {

            if (ITEMS.size() > 1) {
                modelo1 = ITEMS.get(0).getModel();
                modelo2 = ITEMS.get(1).getModel();
                name1 = ITEMS.get(0).getName();
                name2 = ITEMS.get(1).getName();
            }
        }

        public void setValores() {


            if (ITEMS.size() > 1) {

                name_device1.setText(ITEMS.get(0).getName());
                name_device2.setText(ITEMS.get(1).getName());
                model_device1.setText(ITEMS.get(0).getModel());
                model_device2.setText(ITEMS.get(1).getModel());
                year_device1.setText(ITEMS.get(0).getDevYear());
                year_device2.setText(ITEMS.get(1).getDevYear());
                dimensiones1.setText(ITEMS.get(0).getDimensions() + " mm");
                dimensiones2.setText(ITEMS.get(1).getDimensions() + " mm");
                grosor1.setText(ITEMS.get(0).getGrosor() + " mm");
                grosor2.setText(ITEMS.get(1).getGrosor() + " mm");
                peso1.setText(ITEMS.get(0).getPeso() + " g");
                peso2.setText(ITEMS.get(1).getPeso() + " g");
                dual1.setText(ITEMS.get(0).isDualSim());
                dual2.setText(ITEMS.get(1).isDualSim());
                pantalla1.setText(ITEMS.get(0).getPantalla() + "'");
                pantalla2.setText(ITEMS.get(1).getPantalla() + "'");
                dpi1.setText(ITEMS.get(0).getDpi() + " px");
                dpi2.setText(ITEMS.get(1).getDpi() + " px");
                tpantalla1.setText(ITEMS.get(0).getPantallaType());
                tpantalla2.setText(ITEMS.get(1).getPantallaType());
                procesador1.setText(ITEMS.get(0).getProcesador());
                procesador2.setText(ITEMS.get(1).getProcesador());
                ram1.setText(ITEMS.get(0).getRamMemory() + " Gb");
                ram2.setText(ITEMS.get(1).getRamMemory() + " Gb");
                interna1.setText(ITEMS.get(0).getInternalStorage() + " Gb");
                interna2.setText(ITEMS.get(1).getInternalStorage() + " Gb");
                sd1.setText(ITEMS.get(0).isMicroSd());
                sd2.setText(ITEMS.get(1).isMicroSd());
                bateria1.setText(ITEMS.get(0).getBatery() + " mAh");
                bateria2.setText(ITEMS.get(1).getBatery() + " mAh");
                camaraf1.setText(ITEMS.get(0).getCamaraFrontal() + " mp");
                camaraf2.setText(ITEMS.get(1).getCamaraFrontal() + " mp");
                camaraT1.setText(ITEMS.get(0).getCamaraTrasera() + " mp");
                camaraT2.setText(ITEMS.get(1).getCamaraTrasera() + " mp");
                sistema1.setText(ITEMS.get(0).getOperSystem());
                sistema2.setText(ITEMS.get(1).getOperSystem());
                version1.setText(ITEMS.get(0).getSystemVersion());
                version2.setText(ITEMS.get(1).getSystemVersion());
//                precio1.setText(ITEMS.get(0).getPrice() + " CUC");
//                precio2.setText(ITEMS.get(1).getPrice() + " CUC");

                miRecomendacion((ArrayList<Moviles>) ITEMS);
            } else {
                Toast.makeText(getContext(), "No se encontraron dispositivos para comparar. ", Toast.LENGTH_SHORT).show();
                getActivity().finish();

            }


        }

        public void miRecomendacion(ArrayList<Moviles> movil) {
            rec1 = 0;
            rec2 = 0;


//            valoracionMovil(valorarEnteros(Integer.parseInt(ITEMS.get(0).getPrice()), Integer.parseInt(ITEMS.get(1).getPrice()), precio1, precio2, true));
            valoracionMovil(valorarString(ITEMS.get(0).getDpi(), ITEMS.get(1).getDpi(), dpi1, dpi2, false));
            valoracionMovil(valorarString(ITEMS.get(0).getDimensions(), ITEMS.get(1).getDimensions(), dimensiones1, dimensiones2, true));
            valoracionMovil(valorarString(ITEMS.get(0).getSystemVersion(), ITEMS.get(1).getSystemVersion(), version1, version2, false));

            valoracionMovil(valorarFloat(Float.parseFloat(ITEMS.get(0).getCamaraFrontal()), Float.parseFloat(ITEMS.get(1).getCamaraFrontal()), camaraf1, camaraf2, false));
            valoracionMovil(valorarFloat(Float.parseFloat(ITEMS.get(0).getCamaraTrasera()), Float.parseFloat(ITEMS.get(1).getCamaraTrasera()), camaraT1, camaraT2, false));
            valoracionMovil(valorarFloat(Float.parseFloat(ITEMS.get(0).getGrosor()), Float.parseFloat(ITEMS.get(1).getGrosor()), grosor1, grosor2, true));
            valoracionMovil(valorarFloat(Float.parseFloat(ITEMS.get(0).getPantalla()), Float.parseFloat(ITEMS.get(1).getPantalla()), pantalla1, pantalla2, false));
            valoracionMovil(valorarEnteros(Integer.parseInt(ITEMS.get(0).getBatery()), Integer.parseInt(ITEMS.get(1).getBatery()), bateria1, bateria2, false));
            valoracionMovil(valorarEnteros(Integer.parseInt(ITEMS.get(0).getInternalStorage()), Integer.parseInt(ITEMS.get(1).getInternalStorage()), interna1, interna2, false));
            valoracionMovil(valorarFloat(Float.parseFloat(ITEMS.get(0).getRamMemory()), Float.parseFloat(ITEMS.get(1).getRamMemory()), ram1, ram2, false));
            valoracionMovil(valorarFloat(Float.parseFloat(ITEMS.get(0).getPeso()), Float.parseFloat(ITEMS.get(1).getPeso()), peso1, peso2, true));
            valoracionMovil(valorarBoleano(ITEMS.get(0).isDualSim(), ITEMS.get(1).isDualSim(), dual1, dual2));
            valoracionMovil(valorarBoleano(ITEMS.get(0).isMicroSd(), ITEMS.get(1).isMicroSd(), sd1, sd2));

            if (rec1 > rec2) {
                result1.setText(String.valueOf(rec1) + " razones para elegir el '" + ITEMS.get(0).getName() + "'");
                result2.setText(String.valueOf(rec2) + " razones para elegir el '" + ITEMS.get(1).getName() + "'");
            }
            if (rec1 < rec2) {
                result1.setText(String.valueOf(rec2) + " razones para elegir el '" + ITEMS.get(1).getName() + "'");
                result2.setText(String.valueOf(rec1) + " razones para elegir el '" + ITEMS.get(0).getName() + "'");
            }
            if (rec1 == rec2) {
                result1.setText("Xmovil ve un claro empate, la desición es tuya");
            }
            result1.setTextColor(Color.BLACK);


        }

        /**
         * @param valor1
         * @param valor2
         * @param tv_lft
         * @param tv_rgt
         * @return
         */
        public String valorarBoleano(String valor1, String valor2, TextView tv_lft, TextView tv_rgt) {
            String val = "";
            if (valor1.equals("SI")) {
                tv_lft.setTextColor(Color.rgb(83, 177, 88));
                val = "1";
            }
            if (valor2.equals("SI")) {
                tv_rgt.setTextColor(Color.rgb(83, 177, 88));
                val = "2";
            }
            if (valor1.equals("NO")) {
                tv_lft.setTextColor(Color.rgb(255, 153, 51));

            }
            if (valor2.equals("NO")) {
                tv_rgt.setTextColor(Color.rgb(255, 153, 51));
            }
            if (valor1.equals("Si") && valor2.equals("Si")) {
                val = "empate";
                tv_lft.setTextColor(Color.rgb(83, 177, 88));
                tv_rgt.setTextColor(Color.rgb(83, 177, 88));

            }
            return val;
        }

        /**
         * @param valor1
         * @param valor2
         * @param tv_lft
         * @param tv_rgt
         * @param minor
         * @return
         */

        public String valorarFloat(float valor1, float valor2, TextView tv_lft, TextView tv_rgt, boolean minor) {
            String val = "";
            if ((!minor) ? valor1 > valor2 : valor1 < valor2) {
                val = "1";
                tv_lft.setTextColor(Color.rgb(83, 177, 88));
                tv_rgt.setTextColor(Color.rgb(255, 153, 51));
            }
            if ((!minor) ? valor1 < valor2 : valor1 > valor2) {
                val = "2";
                tv_rgt.setTextColor(Color.rgb(83, 177, 88));
                tv_lft.setTextColor(Color.rgb(255, 153, 51));
            }
            if (valor1 == valor2) {
                val = "empate";
                tv_lft.setTextColor(Color.rgb(83, 177, 88));
                tv_rgt.setTextColor(Color.rgb(83, 177, 88));
            }
            return val;
        }

        /**
         * @param valor1
         * @param valor2
         * @param tv_lft
         * @param tv_rgt
         * @return
         */
        public String valorarEnteros(int valor1, int valor2, TextView tv_lft, TextView tv_rgt, boolean minor) {
            String val = "";

            if ((!minor) ? valor1 > valor2 : valor1 < valor2) {
                val = "1";
                tv_lft.setTextColor(Color.rgb(83, 177, 88));
                tv_rgt.setTextColor(Color.rgb(255, 153, 51));
            }
            if ((!minor) ? valor1 < valor2 : valor1 > valor2) {
                val = "2";
                tv_rgt.setTextColor(Color.rgb(83, 177, 88));
                tv_lft.setTextColor(Color.rgb(255, 153, 51));
            }
            if (valor1 == valor2) {
                val = "empate";
                tv_lft.setTextColor(Color.rgb(83, 177, 88));
                tv_rgt.setTextColor(Color.rgb(83, 177, 88));
            }
            return val;
        }


        /**
         * @param valor1
         * @param valor2
         * @param tv_lft
         * @param tv_rgt
         * @param minor
         * @return
         */
        public String valorarString(String valor1, String valor2, TextView tv_lft, TextView tv_rgt, boolean minor) {
            String val = "";
            StringTokenizer rs1 = new StringTokenizer(valor1);
            StringTokenizer rs2 = new StringTokenizer(valor2);
            float token1 = Float.parseFloat(rs1.nextElement().toString());
            float token2 = Float.parseFloat(rs2.nextElement().toString());
            if ((!minor) ? token1 > token2 : token1 < token2) {
                val = "1";
                tv_lft.setTextColor(Color.rgb(83, 177, 88));
                tv_rgt.setTextColor(Color.rgb(255, 153, 51));
            }
            if ((!minor) ? token1 < token2 : token1 > token2) {
                val = "2";
                tv_rgt.setTextColor(Color.rgb(83, 177, 88));
                tv_lft.setTextColor(Color.rgb(255, 153, 51));
            }
            if (token1 == token2) {
                val = "empate";
                tv_lft.setTextColor(Color.rgb(83, 177, 88));
                tv_rgt.setTextColor(Color.rgb(83, 177, 88));
            }


            return val;
        }

        /**
         * @param resultado
         */
        public void valoracionMovil(String resultado) {

            if (resultado.equals("1")) {
                rec1 += 1;
            }
            if (resultado.equals("2")) {
                rec2 += 1;
            }
            if (resultado.equals("empate")) {
                rec1 += 1;
                rec2 += 1;
            }


        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1, model1, model2);
        }

        @Override
        public int getCount() {
            int val = 1;
            // Show 3 total pages.
            if (rutarExist()) {
                val = 3;
            }
            return val;
        }

        public boolean rutarExist() {
            boolean flag = false;
            File ruta_SD = Environment.getExternalStorageDirectory();
            File imgFile = new File(ruta_SD, "/XMOVIL/moviles/");
            if (imgFile.exists()) {
                flag = true;
            }
            return flag;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";
            }
            return null;
        }
    }


}
