package dev.mad.xmovil.models;

import android.app.Activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dev.mad.xmovil.db.MovilDbHelper;
import dev.mad.xmovil.domains.Moviles;

/**
 * Created by Daymel on 16/2/2017.
 */

public class MovilesModel {

    MovilDbHelper miBBDDHelper;
    public static List<Moviles> ITEMS = new ArrayList<Moviles>();
    public static Map<String, Moviles> ITEM_MAP = new HashMap<String, Moviles>();

    public boolean crearBBDD(Activity activity) {
        miBBDDHelper = new MovilDbHelper(activity);
        return miBBDDHelper.comprobarBaseDatos();
    }

    public MovilesModel(Activity activity) {
        crearBBDD(activity);
        miBBDDHelper.abrirBaseDatos();
        ITEMS = miBBDDHelper.getMoviles();

        for (Moviles movil : ITEMS) {
            ITEM_MAP.put(movil.getId(), movil);
        }
    }


}
