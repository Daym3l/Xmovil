package dev.mad.xmovil;


import android.app.Activity;

import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;

import android.graphics.Color;

import android.os.Build;
import android.os.Environment;

import android.support.design.widget.FloatingActionButton;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

import java.util.ArrayList;

import dev.mad.xmovil.db.MovilDbHelper;


public class Principal extends AppCompatActivity {

    MovilDbHelper miBBDDHelper;
    public static java.util.List<String> ITEMS = new ArrayList<String>();

    public static final String app_version = "3.2";


    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        final AutoCompleteTextView campo1 = (AutoCompleteTextView) findViewById(R.id.et_campo1);
        final AutoCompleteTextView campo2 = (AutoCompleteTextView) findViewById(R.id.et_campo2);
        final TextView version = (TextView)findViewById(R.id.tv_version);
        campo1.setInputType(InputType.TYPE_CLASS_TEXT| InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        campo2.setInputType(InputType.TYPE_CLASS_TEXT| InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        campo1.setDropDownWidth(getResources().getDisplayMetrics().widthPixels);
        campo2.setDropDownWidth(getResources().getDisplayMetrics().widthPixels);
        FloatingActionButton fab_about = (FloatingActionButton) findViewById(R.id.fab_about);

        version.setText(app_version);

        fab_about.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                Fragment prev = getFragmentManager().findFragmentByTag("dialog");
                if (prev != null) {
                    ft.remove(prev);
                }
                ft.addToBackStack(null);

                // Create and show the dialog.

                DialogFragment newFragment = MyDialogFragment.newInstance(0);

                newFragment.show(ft, "dialog");


            }
        });

        campo1.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean procesado = false;
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    procesado = true;
                    Comparar(campo1, campo2);
                }
                return procesado;
            }
        });
        campo2.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean procesado = false;
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    procesado = true;
                    Comparar(campo1, campo2);
                }
                return procesado;
            }
        });
        if (this.verificarBdExist()) {
            Toast toast = Toast.makeText(this, "Base de datos cargada con exito. :)", Toast.LENGTH_SHORT);
            toast.show();

        } else {
            TextView descrip = (TextView) findViewById(R.id.tv_xmdescrip);
            descrip.setText("No se pudo localizar la BD, verifique que la carpeta Xmovil con todo su contenido se encuentre en la sd del dispositivo, y reinicie la aplicación");
            descrip.setTextColor(Color.RED);
        }
        if (crearBBDD(Principal.this)) {
            miBBDDHelper.abrirBaseDatos();
            ITEMS = miBBDDHelper.searchNameMovil();
            ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, ITEMS);
            campo1.setAdapter(adapter);
            campo2.setAdapter(adapter);

        }

        Lanzar();
    }

    public boolean crearBBDD(Activity activity) {
        miBBDDHelper = new MovilDbHelper(activity);
        return miBBDDHelper.comprobarBaseDatos();
    }

    /*
    Verificar que exista la memoria SD
     */
    public boolean verifySD() {
        boolean sd_Disponible = false;
        String estado = Environment.getExternalStorageState();

        if (estado.equals(Environment.MEDIA_MOUNTED)) {
            sd_Disponible = true;

        } else {

            sd_Disponible = false;
        }


        return sd_Disponible;
    }




    /**
     *
     */
    public void Lanzar() {
        FloatingActionButton btn_search = (FloatingActionButton) findViewById(R.id.fbtn_accion);
        btn_search.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AutoCompleteTextView model1 = (AutoCompleteTextView) findViewById(R.id.et_campo1);
                AutoCompleteTextView model2 = (AutoCompleteTextView) findViewById(R.id.et_campo2);


                Comparar(model1, model2);


            }
        });

    }

    /**
     * @param model1
     * @param model2
     */
    private void Comparar(AutoCompleteTextView model1, AutoCompleteTextView model2) {
        if (!validarCampos(model1) || !validarCampos(model2)) {
            Log.e("Error", "Campos vacios");


        } else {
            Intent intent = new Intent(getApplicationContext(), TabActivity.class);
            intent.putExtra("model1", model1.getText() + "");
            intent.putExtra("model2", model2.getText() + "");
            startActivity(intent);
        }
    }

    /**
     * @param campo
     * @return
     */
    public boolean validarCampos(EditText campo) {

        boolean flag = true;
        String valor = campo.getText() + "";
        if (valor.equals("")) {
            campo.setHint("No hay dispositivo");
            campo.setHintTextColor(Color.argb(90, 255, 0, 0));
            campo.requestFocus();
            flag = false;
            Toast toast = Toast.makeText(this, "Debe introducir el nombre o el modelo del dispositivo.", Toast.LENGTH_LONG);
            toast.show();
        }
        return flag;
    }

    /*
    Verificar que existe la ruta hacia la BD
     */
    public boolean verificarBdExist() {

        boolean exist = false;
        if (this.verifySD()) {
            try {
                File ruta_SD = Environment.getExternalStorageDirectory();
                File db = new File(ruta_SD.getAbsolutePath(), "XMOVIL/xmovil.db");

                if (db.exists()) {
                    exist = true;

                } else {
                    exist = false;
                    Toast toast = Toast.makeText(this, "No se pudo encontrar la base de datos.", Toast.LENGTH_SHORT);
                    toast.show();
                }

            } catch (Exception ex) {

                Log.e("Ficheros", "No se pudo encontrar la base de datos.");
            }

        } else {
            Toast toast = Toast.makeText(this, "La memoria SD no se encuentra disponible.", Toast.LENGTH_LONG);
            toast.show();

        }
        return exist;
    }
}
