package dev.mad.xmovil.domains;

import java.util.UUID;

/**
 * Created by Daymel on 10/2/2017.
 */

public class Moviles {
    private String id = "";
    private String name = "";
    private String model = "";
    private String devYear = "";
    private String dimensions = "";
    private String grosor = "";
    private String peso = "";
    private String dualSim;
    private String pantalla = "";
    private String pantallaType = "";
    private String dpi = "";
    private String procesador = "";
    private String operSystem = "";
    private String systemVersion = "";
    private String internalStorage = "";
    private String ramMemory = "";
    private String camaraFrontal = "";
    private String camaraTrasera = "";
    private String microSd;
    private String batery = "";
    private String price = "";
    private String fecha = "";

    public Moviles() {
    }

    public Moviles(String name, String model, String devYear, String dimensions, String peso, String dualSim, String pantalla, String pantallaType, String dpi, String procesador, String operSystem, String systemVersion, String internalStorage, String ramMemory, String camaraFrontal, String camaraTrasera, String microSd, String batery, String price, String grosor, String fecha) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.model = model;
        this.devYear = devYear;
        this.dimensions = dimensions;
        this.peso = peso;
        this.dualSim = dualSim;
        this.pantalla = pantalla;
        this.pantallaType = pantallaType;
        this.dpi = dpi;
        this.procesador = procesador;
        this.operSystem = operSystem;
        this.systemVersion = systemVersion;
        this.internalStorage = internalStorage;
        this.ramMemory = ramMemory;
        this.camaraFrontal = camaraFrontal;
        this.camaraTrasera = camaraTrasera;
        this.microSd = microSd;
        this.batery = batery;
        this.price = price;
        this.grosor = grosor;
        this.fecha = fecha;
    }

    public String getGrosor() {
        return grosor;
    }

    public void setGrosor(String grosor) {
        this.grosor = grosor;
    }

    public String getId() {
        return id;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getName() {
        return name;
    }

    public String getModel() {
        return model;
    }

    public String getDevYear() {
        return devYear;
    }

    public String getDimensions() {
        return dimensions;
    }

    public String getPeso() {
        return peso;
    }

    public String isDualSim() {
        String result = "SI";
        if (dualSim.equals("FALSE")) {
            result = "NO";
        }

        return result;
    }

    public String getPantalla() {
        return pantalla;
    }

    public String getPantallaType() {
        return pantallaType;
    }

    public String getDpi() {
        return dpi;
    }

    public String getProcesador() {
        return procesador;
    }

    public String getOperSystem() {
        return operSystem;
    }

    public String getSystemVersion() {
        return systemVersion;
    }

    public String getInternalStorage() {
        return internalStorage;
    }

    public String getRamMemory() {
        return ramMemory;
    }

    public String getCamaraFrontal() {
        return camaraFrontal;
    }

    public String getCamaraTrasera() {
        return camaraTrasera;
    }

    public String isMicroSd() {
        String result = "SI";
        if (microSd.equals("FALSE")) {
            result = "NO";
        }
        return result;
    }

    public String getBatery() {
        return batery;
    }

    public String getPrice() {
        return price;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setDevYear(String devYear) {
        this.devYear = devYear;
    }

    public void setDimensions(String dimensions) {
        this.dimensions = dimensions;
    }

    public void setPeso(String peso) {
        this.peso = peso;
    }

    public void setDualSim(String dualSim) {
        this.dualSim = dualSim;
    }

    public void setPantalla(String pantalla) {
        this.pantalla = pantalla;
    }

    public void setPantallaType(String pantallaType) {
        this.pantallaType = pantallaType;
    }

    public void setDpi(String dpi) {
        this.dpi = dpi;
    }

    public void setProcesador(String procesador) {
        this.procesador = procesador;
    }

    public void setOperSystem(String operSystem) {
        this.operSystem = operSystem;
    }

    public void setSystemVersion(String systemVersion) {
        this.systemVersion = systemVersion;
    }

    public void setInternalStorage(String internalStorage) {
        this.internalStorage = internalStorage;
    }

    public void setRamMemory(String ramMemory) {
        this.ramMemory = ramMemory;
    }

    public void setCamaraFrontal(String camaraFrontal) {
        this.camaraFrontal = camaraFrontal;
    }

    public void setCamaraTrasera(String camaraTrasera) {
        this.camaraTrasera = camaraTrasera;
    }

    public void setMicroSd(String microSd) {
        this.microSd = microSd;
    }

    public void setBatery(String batery) {
        this.batery = batery;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
