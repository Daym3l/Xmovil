package dev.mad.xmovil.db;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;

import dev.mad.xmovil.domains.Moviles;

/**
 * Created by Daymel on 10/2/2017.
 */

public class MovilDbHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    File ruta_SD = Environment.getExternalStorageDirectory();
    String DATABASE_PATH = ruta_SD.getAbsolutePath() + "/XMOVIL/xmovil.db";
    public static final String DATABASE_NAME = "xmovil.db";
    private SQLiteDatabase myDb;
    private final Context myContex;

    public MovilDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.myContex = context;
    }


    public boolean comprobarBaseDatos() {
        SQLiteDatabase checkDB = null;
        try {
            checkDB = SQLiteDatabase.openDatabase(DATABASE_PATH, null,
                    SQLiteDatabase.OPEN_READWRITE);
        } catch (SQLiteException e) {
            Log.e("Basedatos", "No se tiene acceso al fichero de la BD.");

        }

        if (checkDB != null) {
            checkDB.close();
        }

        return checkDB != null ? true : false;
    }

    public void abrirBaseDatos() throws SQLException {
        String myPath = DATABASE_PATH;
        myDb = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READWRITE);

    }

    public synchronized void close() {
        if (myDb != null)
            myDb.close();

        super.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public static final String TABLE_NAME = "moviles";

    public static final String ID = "id_moviles";
    public static final String NAME = "name";
    public static final String MODEL = "model";
    public static final String DEVYEAR = "lanzamiento";
    public static final String DIMENSIONS = "dimensiones";
    public static final String PESO = "peso";
    public static final String DUALSIM = "dual";
    public static final String PANTALLA = "pantalla";
    public static final String PANTALLA_TYPE = "tipopantalla";
    public static final String DPI = "dpi";
    public static final String PROCESADOR = "procesador";
    public static final String OPER_SYSTEM = "os";
    public static final String SYSTEM_VERSION = "vos";
    public static final String INTERNAL_STORAGE = "internal";
    public static final String RAM_MEMORY = "ram";
    public static final String CAMARA_FRONTAL = "camaraf";
    public static final String CAMARA_TRASERA = "camarat";
    public static final String MICROSD = "microsd";
    public static final String BATERY = "bateria";
    public static final String PRICE = "precio";
    public static final String GROSOR = "grosor";


    public ArrayList<Moviles> getMoviles() {
        ArrayList<Moviles> listaTareas = new ArrayList<Moviles>();
        try {
            Cursor c = myDb.query(TABLE_NAME, new String[]{
                            ID, NAME, MODEL, DEVYEAR, DIMENSIONS, PESO, DUALSIM, PANTALLA,
                            PANTALLA_TYPE, DPI, PROCESADOR, OPER_SYSTEM, SYSTEM_VERSION, INTERNAL_STORAGE,
                            RAM_MEMORY, CAMARA_FRONTAL, CAMARA_TRASERA, MICROSD, BATERY, PRICE, GROSOR}, null,
                    null, null, null, null);

// Iteramos a traves de los registros del cursor
            c.moveToFirst();
            while (c.isAfterLast() == false) {
                Moviles movil = new Moviles();
                movil.setName(c.getString(1));
                movil.setModel(c.getString(2));
                movil.setDevYear(c.getString(3));

                listaTareas.add(movil);
                c.moveToNext();
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return listaTareas;
    }

    public ArrayList<Moviles> searchMovil(String device1, String device2) {


        String m1 = (device1 == null) ? " No dispoositivo" : device1.toUpperCase();
        String m2 = (device2  == null) ? " No dispoositivo" : device2.toUpperCase();
        ArrayList<Moviles> resultado = new ArrayList<Moviles>();

        try {
            Cursor c = myDb.rawQuery("SELECT name,model,lanzamiento,dimensions,peso,grosor,dual,pantalla,tipopantalla,dpi,procesador,internal,ram,microsd,bateria,camaraf,camarat,os,vos,precio,updated_at FROM moviles WHERE moviles.model IN " + "('" + m1 + "','" + m2 + "') OR moviles.name IN " + "('" + m1 + "','" + m2 + "')", null);

            c.moveToFirst();

            while (c.isAfterLast() == false) {
                Moviles movil = new Moviles();
                movil.setName(c.getString(0));
                movil.setModel(c.getString(1));
                movil.setDevYear(c.getString(2));
                movil.setDimensions(c.getString(3));
                movil.setPeso(c.getString(4));
                movil.setGrosor(c.getString(5));
                movil.setDualSim(c.getString(6));
                movil.setPantalla(c.getString(7));
                movil.setPantallaType(c.getString(8));
                movil.setDpi(c.getString(9));
                movil.setProcesador(c.getString(10));
                movil.setInternalStorage(c.getString(11));
                movil.setRamMemory(c.getString(12));
                movil.setMicroSd(c.getString(13));
                movil.setBatery(c.getString(14));
                movil.setCamaraFrontal(c.getString(15));
                movil.setCamaraTrasera(c.getString(16));
                movil.setOperSystem(c.getString(17));
                movil.setSystemVersion(c.getString(18));
                movil.setPrice(c.getString(19));
                movil.setFecha(c.getString(20));

                resultado.add(movil);
                c.moveToNext();
            }
            c.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultado;
    }

    public ArrayList<String> searchNameMovil() {

        ArrayList<String> resultado = new ArrayList<String>();
        try {
            Cursor c = myDb.rawQuery("SELECT name,model FROM moviles ORDER BY id_moviles ", null);

            c.moveToFirst();

            while (c.isAfterLast() == false) {
                resultado.add(c.getString(0));
                c.moveToNext();
            }
            c.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultado;
    }
}
