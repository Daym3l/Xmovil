# Xmovil
Android aplication

Aplicación para la comparación de terminales móviles, sencilla y fácil de usar.

## Getting Started

* Tener androidStudio instalado.

* Importar proyecto.

## Version
```3.2.0```

## Authors

* **Daymel Machado Cabrera** - [Daym3l](https://github.com/Daym3l)
